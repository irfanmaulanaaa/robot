*** Settings ***
Library         AppiumLibrary   timeout=30
Library         OperatingSystem
Library         String
Documentation   setting connection



*** Variables ***
#==================== CONNECTION ==========================================
${REMOTE_URL}           http://127.0.0.1:4723/wd/hub
${deviceName}           iPhone 7
# ini udid iphone7b
${udid}                 20344817a83733a79322be125c9c88fad8092798
${xcodeOrgId}           CTC85SS8VB
${xcodeSigningId}       iPhone Developer
${platformVersion}      10.3

#===================== CONFIG COMPANY APPS ============================
# add package and bundleid for new company
${list_comp}   celli  ,  fph  ,  msig  ,  parkway  ,  roche  ,  oompf

&{celli}    bundleid=com.cellihealth.cellihealth
...         package=com.cellihealth.cellihealth
&{fph}      bundleid=com.cellihealth.FarrerPark
...         package=com.cellihealth.farrerpark
&{msig}     bundleid=com.cellihealth.msigwellness
...         package=com.cellihealth.msigwellness
&{parkway}  bundleid=com.cellihealth.parkway
...         package=com.cellihealth.parkway
&{roche}  bundleid=com.cellihealth.rochowellness
...         package=com.cellihealth.rochowellness
&{oompf}  bundleid=com.cellihealth.oompf
...         package=com.cellihealth.oompf



*** Keywords ***
Open App
    Validation Mode Syntax
    Run Keyword If    '${platform}'=='iphone'   Run Iphone
    ...    ELSE IF    '${platform}'=='ios'      Run iOS
    ...    ELSE IF    '${platform}'=='android'  Run Android

    Run Keyword If    '${platform}'=='iphone'   Convert Platform Iphone to iOS

Convert Platform Iphone to iOS
    ${platform}  Set Variable    ios
    Set Global Variable    ${platform}

Run Iphone
    ${bundleId}  Set Variable If
    ...          '${type}'=='prod'   ${${company}.bundleid}
    ...          '${type}'=='dev'    ${${company}.bundleid}.dev
    #setting realdevice change deviceName platformVersion and udid
    Open Application
    ...   ${REMOTE_URL}
    ...   platformName=iOS
    ...   deviceName=iPhone 7
    ...   platformVersion=11.2
    ...   udid=20344817a83733a79322be125c9c88fad8092798
    ...   bundleId=${bundleId}
    ...   xcodeOrgId=${xcodeOrgId}
    ...   xcodeSigningId=${xcodeSigningId}
    ...   noReset=false
    ...   automationName=xcuitest
    Is Login iOS

Run iOS
    ${bundleId}  Set Variable If
    ...          '${type}'=='prod'   ${${company}.bundleid}
    ...          '${type}'=='dev'    ${${company}.bundleid}.dev
    #setting simulator edit deviceName and platformVersion
    Open Application
    ...   ${REMOTE_URL}
    ...   platformName=iOS
    ...   deviceName=iPhone 7
    ...   platformVersion=10.3
    ...   bundleId=${bundleId}
    ...   xcodeOrgId=${xcodeOrgId}
    ...   xcodeSigningId=${xcodeSigningId}
    ...   noReset=false
    ...   automationName=xcuitest
    Is Login iOS

Run Android
    ${package}   Set Variable If
    ...          '${type}'=='prod'   ${${company}.package}
    ...          '${type}'=='dev'    ${${company}.package}.dev
    ${activity}  Set Variable  ${package}.MainActivity
    Open Application
    ...   ${REMOTE_URL}
    ...   platformName=Android
    ...   appPackage=${package}
    ...   appActivity=${activity}
    ...   deviceName=${deviceName}

Error Mode Syntax
    [Arguments]   ${error_message}
    Fail   ${error_message} \n\n robot -v mode:platform.company.type testcase\\version \n\n platform : android , ios, iphone (for real device) \n company : ${list_comp} \n type : dev , prod

Converting Variable Mode
    # split variable mode to be 3 array
    # ${platform} = platform (android/ios)
    # ${company} = company (fph/celli/mmb)
    # ${type} = production or staging
    @{type}  Split String    ${mode}   .
    ${platform}   Set Variable    ${type[0]}
    ${company}    Set Variable    ${type[1]}
    ${type}       Set Variable    ${type[2]}
    Set Global Variable    ${platform}
    Set Global Variable    ${company}
    Set Global Variable    ${type}

Validation Mode Syntax
    ${status}  Run Keyword And Return Status    Converting Variable Mode
    Run Keyword If    ${status}==False    Error Mode Syntax
    Run Keyword Unless    '${platform}'=='android' or '${platform}'=='ios' or '${platform}'=='iphone'   Error Mode Syntax    WRONG PLATFORM !!!
    Run Keyword Unless    '${type}'=='dev' or '${type}'=='prod'   Error Mode Syntax    WRONG TYPE !!!
    Run Keyword Unless    $company in $list_comp   Error Mode Syntax    WRONG COMPANY !!!
    Converting Variable Mode

Is Login iOS
    ${is_login}  Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="What's Up"]
    Run Keyword If    ${is_login}==True    Log Out

Log Out
    AppiumLibrary.Click Element    accessibility_id=menu option
    AppiumLibrary.Wait Until Element Is Visible    accessibility_id=Log Out
    AppiumLibrary.Click Element    accessibility_id=Log Out
