# RobotFramework for Automation Testing

[http://robotframework.org/](http://robotframework.org/)
  
    
### Pre-Requisites 
* node.js
* xcode
* android sdk
* appium 1.6.x
* appium-xcuitest-driver (for iOS)
* pip
* robotframework
* appiumlibrary
* seleniumlibrary
* fakerlibrary

### Installation
- node.js
    ```
    $ brew install node
    ```
- appium
    ```sh
    $ npm install -g appium
    $ npm install -g appium-doctor
    ```
- appium-xcuitest-driver (for iOS)
    ```sh
    $ brew install ideviceinstaller
    $ brew install carthage
    $ npm install -g ios-deploy
    $ npm install -g deviceconsole
    $ gem install xcpretty
    $ brew install libimobiledevice --HEAD
    ```     
- pip  (download https://bootstrap.pypa.io/get-pip.py)
    ```sh
    $ python get-pip.py
    ```    
- robotframework  
    ```sh
    $ pip install robotframework
    ```
 - downgrade selenium to 3.0.0  
    ```sh
    $ pip uninstall selenium
	$ pip install selenium==3.0.0	
    ```
- selenium2library  
    ```sh
    $ pip install robotframework-selenium2library==3.0.0b1
    ```
- appiumlibrary  
    ```sh
    $ pip install robotframework-appiumlibrary
    ```    
- fakerlibrary	
    ```sh
    $ pip install robotframework-faker
    ``` 
- Setting up Webdriver Agent.    
    1.  open terminal
    2.  Go to cd /usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/
    3.  run script `./Scripts/bootstrap.sh`
    4.  Xcode should open
        ![](https://www.dropbox.com/s/ufp0ildf8vrt0kh/wda1.png?raw=1)
    5. setup WebDriverAgent following image below
        - WebDriverAgentLib - General  
          ```Bundle Identifier : com.facebook.WebdDriverAgentLib```
        ![](https://www.dropbox.com/s/il9zf78u7vx42rw/wda2.png?raw=1)
        - WebDriverAgentLib - Build Settings  
          ```product bundle indentifier : com.facebook.WebdDriverAgentLib```   
          ```product name : WebDriverAgentLib```  
        ![](https://www.dropbox.com/s/qlgpn5aiirrvikr/wda3.png?raw=1)
        - WebDriverAgentRunner - General
        ![](https://www.dropbox.com/s/teb1r14h3ufncn9/wda4.png?raw=1)
        - WebDriverAgentRunner - Build Settings  
          ```product bundle indentifier : com.appium.WebdDriverAgentLib```  
          ```product name : WebDriverAgentLib```  
        ![](https://www.dropbox.com/s/qlgpn5aiirrvikr/wda5.png?raw=1)
        - IntegrationApp - General
        ![](https://www.dropbox.com/s/p154hd0t0rp7qa7/wda6.png?raw=1)
        - IntegrationApp - Build Settings  
          ```product bundle indentifier : com.facebook.WebdDriverAgentLib```  
          ```product name : WebDriverAgentLib```  
        ![](https://www.dropbox.com/s/qlgpn5aiirrvikr/wda7.png?raw=1)
> Check if all dependencies of appium are working fine
`$ appium-doctor`
  
    
### How To Run
1. Clone this repo
2. Run appium server
    ```sh
    $ appium -a 127.0.0.1 -p 4723
    ```  
3. Run testcase 
    ``` 
    $ robot -v mode:(platform).(company).(type) version/(number version)/testcases
    ```  
    ##### Environment Configuration
    ###### available platform is :
    - `android` for android platform
    - `ios` for ios platform
    ###### available company is : 
    - `celli`,  for cellihealth
    - `fph`, for farrerpark wellness
    ###### available type is : 
    - `dev`, for Staging environment.  
    - `prod`, for production environment.
    ###### number version : 
    - `3.1.6`, for version 3.1.6
    ###### this is sample command for running testcase with environment configuration:
    ```sh
    $ robot -v mode:android.celli.dev version/3.1.6/testcases
    $ robot -v mode:ios.celli.prod version/3.1.6/testcases
    ```
### How To Inspect Element
1. download appium desktop from http://appium.io/
2. run appium desktop, start server  
    ![](https://www.dropbox.com/s/wargw41435s810s/appium1.png?raw=1)
3. start inspector session  
    ![](https://www.dropbox.com/s/5jbzzef2xyeplxq/appium7.png?raw=1)
4. input desired capabilities  
    ![](https://www.dropbox.com/s/2iwtw3djht66o7z/appium4.png?raw=1)
    - Android
        ```
        {
          "deviceName": "My Phone",
          "udid": "83e5364349465354",
          "platformName": "Android",
          "appPackage": "com.cellihealth.cellihealth.dev",
          "appActivity": "com.cellihealth.cellihealth.dev.MainActivity"
        }
        ```
    - iOS
        ```
        {
          "deviceName": "iPhone",
          "udid": "20344817a83733a79322be125c9c88fad8092798",
          "platformName": "iOS",
          "automationName": "XCUITest",
          "platformVersion": "11.1.2",
          "xcodeOrgId": "CTC85SS8VB",
          "xcodeSigningId": "iPhone Developer",
          "bundleId": "com.cellihealth.FarrerPark.dev"
        }
        ```
5.  click start session after input desired capabilities,    
    - Android
        ![](https://www.dropbox.com/s/gdpdicc1wu0kp39/appium6.png?raw=1)
    - iOS
        ![](https://www.dropbox.com/s/tj4ass2gryewqg5/appium5.png?raw=1)
