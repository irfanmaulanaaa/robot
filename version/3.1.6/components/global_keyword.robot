*** Settings ***
Library  AppiumLibrary  timeout=30
Library  OperatingSystem
Library  DateTime
Library  String

Resource    ../../../config/app_setup.robot



*** Variables ***
&{button_login}     android=//*[@text="LOGIN"]
...                 ios=//XCUIElementTypeOther[@name="LOGIN"][1]
&{ok_alert}         android=//*[@text='OK']
...                 ios=accessibility_id=OK


*** Keywords ***
capture screen
    [Arguments]   ${file_name}
    ${date}   Get Time
    ${date}   Evaluate    '${date}'.replace(' ','_')
    ${date}   Evaluate    '${date}'.replace(':','.')
    AppiumLibrary.Capture Page Screenshot   screenshot/${platform}/${file_name}_${date}.png

wait splash screen
    AppiumLibrary.Wait Until Element Is Visible   ${button_login.${platform}}

click login
    AppiumLibrary.Click Element     ${button_login.${platform}}

close popup message
    AppiumLibrary.Click Element    ${ok_alert.${platform}}
