*** Settings ***
Library  AppiumLibrary  timeout=30
Library  OperatingSystem
Library  DateTime
Library  String


Resource    ../../../config/app_setup.robot



*** Variables ***
&{button_login}     android=//*[@text="LOGIN"]
...                 ios=//XCUIElementTypeOther[@name="LOGIN"][1]
&{input_email}      android=accessibility_id=input email
...                 ios=accessibility_id=input email
&{input_pass}       android=accessibility_id=input password
...                 ios=accessibility_id=input password
# &{show_pass}        android=//android.view.ViewGroup[@index='6']
# ...                 ios=//XCUIElementTypeOther[@name=" "]
&{submit_login}     android=//*[@text='SUBMIT']
...                 ios=//XCUIElementTypeOther[@name="SUBMIT"]
&{all_field}        android=id=message
...                 ios=accessibility_id=All fields are required!
&{email_format}     android=id=message
...                 ios=accessibility_id=Email format is invalid!
&{invalid_format}   android=id=message
...                 ios=accessibility_id=Invalid Email or Password.
&{ok_alert}         android=//*[@text='OK']
...                 ios=accessibility_id=OK
&{success_login}    android=//*[@text="What's Up"]
...                 ios=//XCUIElementTypeStaticText[@name="What's Up"]

*** Keywords ***
capture screen
    [Arguments]   ${file_name}
    ${date}   Get Time
    ${date}   Evaluate    '${date}'.replace(' ','_')
    ${date}   Evaluate    '${date}'.replace(':','.')
    Capture Page Screenshot   screenshot/${platform}/${file_name}_${date}.png

wait splash screen
    Wait Until Element Is Visible   ${button_login.${platform}}

click login
    Click Element     ${button_login.${platform}}

error message all field are required!
    Wait Until Element Is Visible   ${all_field.${platform}}
    Element Text Should Be    ${all_field.${platform}}    All fields are required!

error message email format is invalid!
    Wait Until Element Is Visible   ${email_format.${platform}}
    Element Text Should Be    ${email_format.${platform}}    Email format is invalid!

error message invalid email or password.
    Wait Until Element Is Visible   ${invalid_format.${platform}}
    Element Text Should Be    ${invalid_format.${platform}}    Invalid Email or Password.

success login
    #check success login with check element manage program
    Wait Until Element Is Visible    ${success_login.${platform}}

close popup message
    Click Element    ${ok_alert.${platform}}

# for login
input email and password
    [Arguments]   ${username}   ${pass}
    #input email
    Wait Until Element Is Visible    ${input_email.${platform}}
    Clear Text    ${input_email.${platform}}
    Input Text    ${input_email.${platform}}   ${username}
    #input password and show password
    Clear Text    ${input_pass.${platform}}
    Input Text    ${input_pass.${platform}}    ${pass}
    # Click Element    ${show_pass.${platform}}
    Click Element    ${submit_login.${platform}}
