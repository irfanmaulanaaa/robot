*** Settings ***
Library  OperatingSystem
Library  DateTime
Library  String
Library  Selenium2Library
Library  Dialogs
Library  FakerLibrary


Resource    ../../../config/app_setup.robot
Resource    ../../../version/3.1.6/components/global_keyword.robot


# *** Test Cases ***
# create new user
#     Open chrome
#     goto invitation page
#     input user
#     save data new user
#     Close Browser


*** Keywords ***

#==========================================================================
#create new user from backoffice
Open chrome
    Open Browser    http://staging.cellihealth.com/site/login   googlechrome
    Selenium2Library.Input Text     name=username    admincelli
    Selenium2Library.Input Text     name=password    qa2017
    Execute Manual Step    please input captcha
    Selenium2Library.Click Button   //*[@class="btn btn-success uppercase"]

goto invitation page
    Selenium2Library.Wait Until Page Contains     All Invitations
    Click Link     All Invitations
    Selenium2Library.Wait Until Page Contains     New Invitation
    Click Link     New Invitation

input user
    #input new user
    ${firstname}    First Name
    ${new_email}        Catenate    ${firstname}@robot.com
    ${new_email}        Convert To Lowercase    ${new_email}
    Selenium2Library.Input Text      name=first_name    ${firstname}
    Selenium2Library.Input Text      name=last_name     robot
    Selenium2Library.Input Text      name=email         ${new_email}
    Selenium2Library.Click Button    id=btnSubmit


save data new user
    #get email & password from first row user
    Selenium2Library.Wait Until Page Contains    Send to All
    ${new_email}     Get Table Cell    xpath=//table[@id="div_tbl"]    2    5
    ${new_pass}         Get Table Cell    xpath=//table[@id="div_tbl"]    2    6
    Set Global Variable    ${new_email}
    Set Global Variable    ${new_pass}
    Log To Console    ${new_email}
    Log To Console    ${new_pass}
