*** Settings ***
Library  AppiumLibrary  timeout=30
Library  OperatingSystem
Library  DateTime
Library  String


Resource    ../../../config/app_setup.robot
Resource    ../../../version/3.1.6/components/global_keyword.robot



*** Variables ***
&{button_login}     android=//*[@text="LOGIN"]
...                 ios=//XCUIElementTypeOther[@name="LOGIN"][1]
&{input_email}      android=accessibility_id=input email
...                 ios=accessibility_id=input email
&{input_pass}       android=accessibility_id=input password
...                 ios=accessibility_id=input password
&{submit_login}     android=//*[@text='SUBMIT']
...                 ios=//XCUIElementTypeOther[@name="SUBMIT"]
&{page_reg}         android=//*[@text='Account Details']
...                 ios=//XCUIElementTypeStaticText[@name="Account Details"]
&{reg_pass}         android=accessibility_id=password
...                 ios=accessibility_id=password
&{reg_confpass}     android=accessibility_id=confirmpassword
...                 ios=accessibility_id=confirmpassword
&{reg_gender}       android=accessibility_id=gender
...                 ios=accessibility_id=gender
&{reg_birthdate}    android=accessibility_id=birthdate
...                 ios=accessibility_id=birthdate
&{reg_checkmark}    android=accessibility_id=checkbox
...                 ios=accessibility_id=checkbox
&{reg_submit}       android=//*[@text='SUBMIT']
...                 ios=accessibility_id=SUBMIT
&{alert_pass}       android=id=message
...                 ios=accessibility_id=Password doesn't match!
&{alert_term}       android=id=message
...                 ios=accessibility_id=Please view and agree with our Terms of Use and Privacy Policy.


*** Keywords ***
# input new user and pass
input new email and password
    [Arguments]   ${username}   ${pass}
    AppiumLibrary.Wait Until Element Is Visible    ${input_email.${platform}}
    AppiumLibrary.Input Text    ${input_email.${platform}}   ${username}
    AppiumLibrary.Input Text    ${input_pass.${platform}}    ${pass}
    AppiumLibrary.Click Element    ${submit_login.${platform}}

wait page registration
    AppiumLibrary.Wait Until Element Is Visible    ${page_reg.${platform}}

input password
    [Arguments]   ${new_password}
    AppiumLibrary.Wait Until Element Is Visible   ${reg_pass.${platform}}
    AppiumLibrary.Input Text     ${reg_pass.${platform}}   ${new_password}
    AppiumLibrary.Hide Keyboard

input confirm password
    [Arguments]   ${confirm_password}
    AppiumLibrary.Wait Until Element Is Visible   ${reg_pass.${platform}}
    AppiumLibrary.Input Text     ${reg_confpass.${platform}}   ${confirm_password}
    AppiumLibrary.Hide Keyboard

error message password doesn't match!
    AppiumLibrary.Wait Until Element Is Visible   ${alert_pass.${platform}}
    AppiumLibrary.Element Text Should Be    ${alert_pass.${platform}}    Password doesn't match!


select gender ios
    AppiumLibrary.Click Element    ${reg_gender.${platform}}
    Run Keyword If    '${select_gender}'=='female'    AppiumLibrary.Swipe By Percent    50    90    0    85
    Run Keyword If    '${select_gender}'=='male'      AppiumLibrary.Swipe By Percent    50    90    0    80
    AppiumLibrary.Click Element    //XCUIElementTypeStaticText[@name="Done"]

select gender android
    AppiumLibrary.Click Element    ${reg_gender.${platform}}
    Run Keyword If    '${select_gender}'=='female'   AppiumLibrary.Click Element    //*[@text="Female"]
    Run Keyword If    '${select_gender}'=='male'     AppiumLibrary.Click Element    //*[@text="Male"]

select gender
    [Arguments]   ${select_gender}
    AppiumLibrary.Wait Until Element Is Visible    ${reg_gender.${platform}}
    Set Global Variable    ${select_gender}
    Run Keyword If    '${platform}'=='ios'        select gender ios
    Run Keyword If    '${platform}'=='android'    select gender android

input birthdate ios
    AppiumLibrary.Click Element    ${reg_birthdate.${platform}}
    AppiumLibrary.Swipe By Percent    50    90    0    95
    AppiumLibrary.Click Element    //XCUIElementTypeStaticText[@name="Done"]

input birthdate android
    AppiumLibrary.Click Element    ${reg_birthdate.${platform}}
    AppiumLibrary.Click Element    //*[@text="OK"]

input birthdate
    AppiumLibrary.Wait Until Element Is Visible    ${reg_birthdate.${platform}}
    Run Keyword If    '${platform}'=='ios'        input birthdate ios
    Run Keyword If    '${platform}'=='android'    input birthdate android

select checkmark
    AppiumLibrary.Wait Until Element Is Visible    ${reg_checkmark.${platform}}
    AppiumLibrary.Click Element    ${reg_checkmark.${platform}}

error message please view and agree with our Terms of Use and Privacy Policy.
    AppiumLibrary.Wait Until Element Is Visible   ${alert_term.${platform}}
    AppiumLibrary.Element Text Should Be    ${alert_term.${platform}}    Please view and agree with our Terms of Use and Privacy Policy.

click submit
    ${show_submit}  Run Keyword And Return Status    AppiumLibrary.Element Should Be Visible    ${reg_submit.${platform}}
    Run Keyword If    '${show_submit}'=='False'    AppiumLibrary.Swipe By Percent    50    90    0    70
    AppiumLibrary.Wait Until Element Is Visible    ${reg_submit.${platform}}
    AppiumLibrary.Click Element    ${reg_submit.${platform}}
