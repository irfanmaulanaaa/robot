*** Settings ***
Library     AppiumLibrary  timeout=30
Resource    ../../../config/app_setup.robot
Resource    ../../../version/3.1.6/components/login_keyword.robot

Suite Setup     Open App
Suite Teardown  Close Application

Documentation    testing for check login with success and failed condition

*** Variables ***

*** Test Cases ***
Opening application
    wait splash screen
    capture screenshot
    click login


Login with empty email and password
    [Tags]  negative test   login failed
    input email and password    ${EMPTY}  ${EMPTY}
    error message all field are required!
    capture screenshot
    close popup message

Login with empty email
    [Tags]  negative test   login failed
    input email and password    ${EMPTY}  qa2017
    error message all field are required!
    capture screenshot
    close popup message

Login with empty password
    [Tags]  negative test    login failed
    input email and password   test@qa.com  ${EMPTY}
    error message all field are required!
    capture screenshot
    close popup message

Login with invalid email
    [Tags]  negative test    login failed
    input email and password   test  qa2017
    error message email format is invalid!
    capture screenshot
    close popup message

Login with invalid password
    [Tags]  negative test    login failed
    input email and password   test@qa.com  qa2000
    error message invalid email or password.
    capture screenshot
    close popup message

Login with valid email and password
    [Tags]  positive test    login success
    input email and password   test@qa.com  qa2017
    success login
    capture screenshot


*** Keywords ***
#capture screen (testcase name)
capture screenshot
    capture screen    login
