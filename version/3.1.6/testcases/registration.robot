*** Settings ***
Library     AppiumLibrary  timeout=30
Resource    ../../../config/app_setup.robot
Resource    ../../../version/3.1.6/components/global_keyword.robot
Resource    ../../../version/3.1.6/components/registration_keyword.robot
Resource    ../../../version/3.1.6/components/createuser_keyword.robot

Suite Setup     Open App
# Suite Teardown  Close Application

Documentation    registration testcases

*** Variables ***

*** Test Cases ***
Create new user
    Open chrome
    goto invitation page
    input user
    save data new user
    Close Browser


Opening application
    wait splash screen
    capture screenshot
    click login
    input new email and password    ${new_email}    ${new_pass}
    wait page registration

Password doesn't match
    [Tags]  negative testing    registration failed
    input password    qa2017
    input confirm password    qa20
    select gender    male
    input birthdate
    click submit
    error message password doesn't match!
    capture screenshot
    close popup message

Checkmark term of use
    [Tags]  negative testing    registration failed
    input password    qa2017
    input confirm password    qa2017
    select gender    male
    input birthdate
    click submit
    error message please view and agree with our Terms of Use and Privacy Policy.
    capture screenshot
    close popup message

Success registration
    [Tags]  negative testing    registration success
    input password    qa2017
    input confirm password    qa2017
    select gender    male
    input birthdate
    select checkmark
    capture screenshot
    click submit


*** Keywords ***
#capture screen (testcase name)
capture screenshot
    capture screen    registration
